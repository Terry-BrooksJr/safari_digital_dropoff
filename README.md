<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- PROJECT SHIELDS -->

<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

[Contributors][contributors-url]
[Forks][forks-url]
[Stargazers][stars-url]
[Issues][issues-url]
[MIT License][license-url]
[LinkedIn][linkedin-url]

<!-- PROJECT LOGO -->

<br />
<div align="center">
  <a href="https://github.com/Terry-BrooksJr/safari_digital_dropoff">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Safari Life Digital Drop Off (Special Delivery)</h3>

<p align="center">
    project_description
    <br />
    <a href="https://github.com/Terry-BrooksJr/safari_digital_dropoff"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/Terry-BrooksJr/safari_digital_dropoff">View Demo</a>
    ·
    <a href="https://github.com/Terry-BrooksJr/safari_digital_dropoff/issues">Report Bug</a>
    ·
    <a href="https://github.com/Terry-BrooksJr/safari_digital_dropoff/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

Here's a blank template to get started: To avoid retyping too much info. Do a search and replace with your text editor for the following: `terry-brooksjr`, `safari_life_digital_dropoff`, `twitter_handle`, `terryabrooks`, `Terry.Arthur@BrooksJr.com`, `email`, `Safari Life Digital Drop Off (Special Delivery)`, `project_description`

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

* [Next.js](https://nextjs.org/)
* [React.js](https://reactjs.org/)
* [Vue.js](https://vuejs.org/)
* [Angular](https://angular.io/)
* [Svelte](https://svelte.dev/)
* [Laravel](https://laravel.com)
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

- Python 3+
- Django
- MySQL Database

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/Terry-BrooksJr/safari_digital_dropoff.git
   ```
2. Create Virtual Environment
   ```sh
   pipenv shell
   ```
3. Install PIP packages
   ```sh
   pip install -r requirements.txt
   ```
4. Make Initial Migration
   ```sh
   python ./digital_dropoff/manage.py makemigrations && python ./digital_dropoff/manage.py migrate
   ```
5. Run Development Server
   ```sh
   <COMMAND TO RUN SERVER>
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

## Usage

This web application will allow users to check-in and mark a student picked up by leveraging a secure pin entered by the parent to ensure they are authorized  to remove the child from care.

<p align="right">(<a href="#top">back to top</a>)</p>

### Database Structure

![1676401745664](image/README/1676401745664.png)

`<!-- ROADMAP -->`

## Roadmap

- [ ] Reporting
- [ ] SMS Push Notices to Staff
- [ ] SMS Push Notices to Parents

See the [open issues](hhttps://github.com/Terry-BrooksJr/safari_digital_dropoff/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Terry Brooks - Terry@BrooksJr.com

Project Link: [https://github.com/Terry-BrooksJr/safari_digital_dropoff](https://github.com/Terry-BrooksJr/safari_digital_dropoff)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

* []()
* []()
* []()

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/github/contributors/terry-brooksjr/safari_life_digital_dropoff.svg?style=for-the-badge
[contributors-url]: https://github.com/Terry-BrooksJr/safari_digital_dropoff/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/terry-brooksjr/safari_life_digital_dropoff.svg?style=for-the-badge
[forks-url]: https://github.com/Terry-BrooksJr/safari_digital_dropoff/network/members
[stars-shield]: https://img.shields.io/github/stars/terry-brooksjr/safari_life_digital_dropoff.svg?style=for-the-badge
[stars-url]: https://github.com/Terry-BrooksJr/safari_digital_dropoff/stargazers
[issues-shield]: https://img.shields.io/github/issues/terry-brooksjr/safari_life_digital_dropoff.svg?style=for-the-badge
[issues-url]: https://github.com/Terry-BrooksJr/safari_digital_dropoff/issues
[license-shield]: https://img.shields.io/github/license/terry-brooksjr/safari_life_digital_dropoff.svg?style=for-the-badge
[license-url]: https://github.com/Terry-BrooksJr/safari_digital_dropoff/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/terryabrooks
[product-screenshot]: images/screenshot.png
