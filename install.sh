#!/bin/bash

if pipenv install -r requirements.txt;
then
echo "✔ Requirements Installed in Virtual Environment"
else
echo "Requirements Not Installed in Virtual Environment"
fi 

if pip install -r requirements.txt;
then
echo "✔ Requirements Installed"
else
echo "Requirements Not Installed"
fi 

if pipenv shell;
then 
echo "✔ Environment is Ready"
else
echo "Environment is Not Ready"
fi

