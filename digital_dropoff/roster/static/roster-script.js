const $studentItem = $('.student-item');
const $page = $('.page');
const $pagination = $('.pagination');
const $paginationList = $('.pagination__list');
const $studentSearch = $('.student__search');
const itemTotal = 10;

// hide all 
function hideAll() {
  $studentItem.hide();
}


// display first 10
function displayRange(a, b) {
  hideAll();
  // display 0 - 1 students
  $studentItem.slice(a, b).fadeIn();

}

displayRange(0, itemTotal);

// create pagination links
let pagination = '';
for (var i = 0; i <= $studentItem.length / 10 - 1; i++) {
  pagination += `
    <li><span class ="pagination__num">${i}</span></li>
`;
}
$paginationList.append(pagination);
// click on pagination num
// pass into display range
// calc and show range
$('body').on('click', '.pagination__num', function () {

  hideAll();

  // get text number 1 - 5
  // get ranges for start and end
  let paginationText = Number($(this).text());
  let startFrom = paginationText * itemTotal + paginationText;
  let end = paginationText * itemTotal + paginationText + itemTotal;

  // display ranges
  displayRange(startFrom, end);

});


$studentSearch.on('input', function () {

  hideAll();

  $studentItem.each(function () {
    $(this).removeClass("result");

  });


  // value of searched
  var text = $(this).val().toLowerCase();
  // results of search
  var results = $("ul.student-list li:contains('" + text.toLowerCase() + "')");

  results.addClass("result");


  // if student has result class 
  // dispaly
  // else hide


  if ($studentItem.hasClass('result')) {
    $('.result').show();
    $studentItem.removeClass('result');

  }


});

$studentSearch.keyup(function () {
  if (!this.value) {
    hideAll();
    displayRange(0, itemTotal);
  }

});

$(document).ready(function () {
  const tabs = $('.tab').click(function () {
    if (this.id == 'all') {
      $('.student-item').fadeIn(450);
    } else {
      this.classList.add('active');
      tabs.not(this).removeClass('active');
      const el = $('.' + this.id).fadeIn(450);
      $('.student-item').not(el).hide();
    }
  });
});

// function myFunction() {
//   // Declare variables
//   var input, filter, ul, li, a, i, txtValue;
//   input = document.getElementById('myInput');
//   filter = input.value.toUpperCase();
//   ul = document.getElementById("myUL");
//   li = ul.getElementsByTagName('li');

//   // Loop through all list items, and hide those who don't match the search query
//   for (i = 0; i < li.length; i++) {
//     a = li[i].getElementsByTagName("h3")[0]; txtValue = a.textContent || a.innerText; if
//       (txtValue.toUpperCase().indexOf(filter) > -1) {
//       li[i].style.display = "";
//     } else {
//       li[i].style.display = "none";
//     }
//   }
// }