from django.db import models
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import BaseUserManager, AbstractUser
from django.utils.translation import gettext_lazy as _


class Sex(models.TextChoices):
    MALE = "M", _("Male")
    FEMALE = "F", _("Female")
    NON_BINARY = "X", _("Non-Binary")


def hash_pin(instance_pin):
    if len(instance_pin) != 4:
        raise ValueError("Please Ensure Pin Is 4 Digits")
    else:
        return make_password(instance_pin)


class Student(models.Model):
    student_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    sex = models.CharField(max_length=1, choices=Sex.choices, default=Sex.NON_BINARY)
    address = models.CharField(max_length=200, default="1 World Trade Center")
    city = models.CharField(max_length=200, default="New York")
    state = models.CharField(max_length=200, default="NY")
    contact_number = models.CharField(max_length=255, default="0000000000")
    guardian = models.CharField(max_length=200)
    facility = models.ForeignKey("CareFacility", on_delete=models.PROTECT)
    date_of_birth = models.DateField()
    pickup_pin = models.CharField(max_length=255, default=0000)
    in_care = models.BooleanField()

    def check_pickup_pin(self, pin_to_verify):
        return check_password(pin_to_verify, self.pickup_pin)

    def __str__(self):
        return f"{self.last_name}, {self.first_name} - {self.student_id}"

    def __repr__(self):
        return self.last_name, self.first_name - self.student_id


class Staff(AbstractUser):
    username = models.CharField(max_length=20, unique=True)
    user_id = models.AutoField(primary_key=True)
    password = models.CharField(max_length=256)
    care_facility = models.ForeignKey(
        "CareFacility", on_delete=models.PROTECT, null=True
    )
    last_date_modified = models.DateField(auto_now_add=True)
    role = models.CharField(max_length=256)

    UUSERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    def __str__(self):
        return f"{self.last_name}, {self.first_name} ({self.username})"

    def __repr__(self):
        return ({self.user_last_name}, {self.user_first_name} - {self.user_id})


class CareFacility(models.Model):
    facility_id = models.CharField(primary_key=True, max_length=255, null=False)
    facility_type = models.CharField(max_length=256, null=False)
    # director = models.ForeignKey("Staff", on_delete=models.PROTECT)
    facility_street_address = models.CharField(max_length=256, null=False)
    facility_street_address_2 = models.CharField(max_length=256, null=True)
    facility_city = models.CharField(max_length=256, null=False)
    facility_state = models.CharField(max_length=256, null=False)
    facility_zip = models.CharField(max_length=256, null=False)

    def __str__(self):
        return f"{self.facility_type} - {self.facility_name} - {self.facility_id}"

    def __repr__(self):
        return (self.facility_type, self.facility_name, self.facility_id)
