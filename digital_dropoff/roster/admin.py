from django.contrib import admin
from .models import Student, Staff, CareFacility

# Register your models here.
admin.site.register(Student)
admin.site.register(Staff)
admin.site.register(CareFacility)
