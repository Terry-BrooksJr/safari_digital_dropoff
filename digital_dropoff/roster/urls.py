from django.urls import path
from . import views

urlpatterns = [
    path("roster/", views.roster, name="roster"),
    path("student/<int:pk>/", views.display_student_details, name="student_details"),
]
