from django.shortcuts import render
from .models import Student, CareFacility
from django.http import HttpResponse, HttpRequest
from special_delivery.models import DropOff
import pendulum

def roster(request: HttpRequest) -> HttpResponse:
    students = Student.objects.all().order_by("last_name")
    total_in_care = Student.objects.filter(in_care=True).count()
    total_not_in_care = Student.objects.filter(in_care=False).count()
    roster = {
        "students": students,
        "total_in_care": total_in_care,
        "total_not_in_care": total_not_in_care,
        "title": "Roster",
    }
    return render(request, "roster.html", roster)


def display_student_details(request: HttpRequest, pk=None) -> HttpResponse:
    if pk:
        student = Student.objects.get(student_id=pk)
        facility = CareFacility.objects.get(pk=student.facility_id)
        dropoffs = DropOff.objects.filter(student_id=pk).values('event_id',"event_date", "event_time")
        # dropoff= {
        #     "date": pendulum.date(dropoffs['event_date']).to_date_string(),
        #     "time": pendulum.date(dropoffs['event_time']).to_time_string()
        # }
    else:
        student = ""
    return render(
        request,
        "profile.html",
        {"student": student, "facility": facility, "title": "Student Profile", "dropoffs": dropoffs},
    )
