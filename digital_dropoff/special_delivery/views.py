import json
import pathlib


import pendulum
from django.db import IntegrityError
from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpRequest
from roster.models import Student

from .models import DropOff, PickUp


def check_student_in(request: HttpRequest, pk) -> HttpResponse:
    student = Student.objects.get(student_id=pk)
    student_dict = student.__dict__
    if student.in_care == True:
        student_dict.update({"success": False})
        student_dict.update({"already_checked_in": True})
        return render(request, "check_in_status.html", student_dict)
    else:
        try:
            dropoff = DropOff(student=student)
            dropoff.save()
            dropoff = dropoff.__dict__
            student_dict.update({"success": True})
            student_dict.update({"dropoff": dropoff})
            student.in_care = True
            student.save()
            print(student_dict)
            return render(request, "check_in_status.html", student_dict)
        except IntegrityError:
            student_dict.update({"success": False})
            student_dict.update({"duplicate_checkout": True})
            print(student_dict)
            return render(request, "check_in_status.html", student_dict)


def correct_pin(request: HttpRequest, submitted_pin: str, pk: str) -> HttpResponse:
    if request.method == "POST":
        body_unicode = request.body.decode("utf-8")
        payload = json.loads(body_unicode)["pin"]
        pin = payload["pin"]
        pk = get_id_from_slug(payload["url"])
        student = Student.objects.get(student_id=pk)
        student_dict = student.__dict__
        student_pin = student["pin"]
        print("D")
        if student.in_care == True:
            print("E")
            if submitted_pin == student_pin:
                student.in_care = False
                student.save()
                pickup = PickUp(student=student)
                pickup.save()
                pickup = pickup.__dict__
                student_dict.update({"pickup": pickup})
                student_dict.update({"success": True})
                return render(request, "check_out_status.html", student_dict)
            else:
                student_dict.update({"success": False})
                student_dict.update({"incorrect_pin": True})
                return render(request, "check_out_status.html", student_dict)


def check_student_out(request: HttpRequest, pk: str) -> HttpResponse:
    student = Student.objects.get(student_id=pk)
    student_dict = student.__dict__
    # return render(request, 'release_student.html', student_dict)
    if request.method == "GET":
        print("A")
        return render(request, "release_student.html", student_dict)
    else:
        print("F")
        return HttpResponse(status=405)
