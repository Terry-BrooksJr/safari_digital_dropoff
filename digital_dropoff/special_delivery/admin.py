from django.contrib import admin
from .models import DropOff, PickUp

# Register your models here.
admin.site.register(DropOff)
admin.site.register(PickUp)

