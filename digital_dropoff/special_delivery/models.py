from django.db import models
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.utils.timezone import is_aware, make_aware


class PickUp(models.Model):
    event_id = models.AutoField(primary_key=True)
    student = models.ForeignKey("roster.Student", on_delete=models.CASCADE)
    event_date = models.DateField(auto_now_add=True)
    event_time = models.TimeField(auto_now_add=True)
    late = models.BooleanField(default=False)
    
    def __str__(self):
        Student = self.student.name
        return f"{self.s}"

    class Meta:
        get_latest_by = ["event_date", "event_time"]
        ordering = ["event_date", "event_time"]
        unique_together = ["event_date", "student"]


class DropOff(models.Model):
    def get_aware_datetime(self, date_str):
        ret = parse_datetime(date_str)
        if not is_aware(ret):
            ret = make_aware(ret)
        return ret

    event_id = models.AutoField(primary_key=True)
    student = models.ForeignKey("roster.Student", on_delete=models.CASCADE)
    event_date = models.DateField(auto_now_add=True)
    event_time = models.TimeField(default=timezone.now)

    class Meta:
        get_latest_by = ["event_date", "event_time"]
        ordering = ["event_date", "event_time"]
        unique_together = ["event_date", "student"]
