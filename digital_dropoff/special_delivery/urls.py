from django.urls import path
from . import views

urlpatterns = [
    path("check-in/<int:pk>", views.check_student_in, name="checkin"),
    path("release/<int:pk>", views.check_student_out, name="check_student_out"),
    path("verify", views.correct_pin, name="verify_pin"),
]
