from django import forms


class Pin(forms.Form):
    pin = forms.CharField(max_length=4, min_length=4, label="PIN")
