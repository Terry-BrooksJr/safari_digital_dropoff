import os
import time

import datapane as dp
import pandas as pd
from django.http import FileResponse, HttpRequest, HttpResponse, StreamingHttpResponse
from django.shortcuts import redirect, render
from get_port import find_free_port, port_available
from pendulum import now
from roster.models import Student

from digital_dropoff.settings import BASE_DIR

from . import utils
from .forms import BasicReportForm, DetailedReportForm

now = now()


def basic_report(request: HttpRequest):
        form = BasicReportForm()
        return render(
            request, "basic_report.html", {"title": "Basic Report", "form": form}
        )


def detailed_report(request: HttpRequest) -> HttpResponse:
    return render(request, "detailed_report.html", {"title": "Detailed Report"})


def report_manager(request: HttpRequest) -> HttpResponse:
 form = BasicReportForm(request.POST)
 if form.is_valid():
    report_type = form.cleaned_data.get("report_type")
    if report_type == "IC":
        queryset = (
        Student.objects.filter(in_care=True).values("first_name", "last_name", "student_id", "guardian", "contact_number").order_by("last_name", "first_name",)
    )
        report_title = "In Care Report"
    elif report_type == "NIC":
        queryset = Student.objects.filter(in_care=False).values("first_name", "last_name", "student_id", "guardian", "contact_number").order_by("last_name", "first_name",)
        report_type = "Not In Care Report"
    elif report_type == "LPR":
        queryset = None
        report_title = "Late Pickup Report"
    elif report_type == "AES":
        queryset = Student.objects.all().values("first_name", "last_name", "student_id", "guardian", "contact_number").order_by("last_name", "first_name",)
        report_title = "All Enrolled Students"
    df = pd.DataFrame(queryset)
    port = find_free_port()[0]
    report = dp.App(
        dp.HTML('<h1>Safari Life Reporting</h1>'),
        dp.HTML(f'<h2>{report_title}</h2>'),
        dp.HTML('<a class="nav-link" target="_blank" href="http://localhost:8000/roster">Back to Roster</a>'),
        dp.Divider(),
        dp.DataTable(df))
    return StreamingHttpResponse (
    dp.serve(
        app=report,
        name="report",
        port=port,
        overwrite=True,
        open=True,
        host="localhost",
        dest="./report",
    ),  content_type="text/html")
