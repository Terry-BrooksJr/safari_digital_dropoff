from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, SimpleDocTemplate, Spacer, Table, TableStyle
import io
import csv
import pandas as pd
import json

import pendulum

now = pendulum.now()


def get_model_field_names(model, ignore_fields=["content_object"]):
    """
    ::param model is a Django model class
    ::param ignore_fields is a list of field names to ignore by default
    This method gets all model field names (as strings) and returns a list
    of them ignoring the ones we know don't work (like the 'content_object' field)
    """
    model_fields = model._meta.get_fields()
    model_field_names = list(
        set([f.name for f in model_fields if f.name not in ignore_fields])
    )
    return model_field_names


def get_lookup_fields(model, fields=None):
    """
    ::param model is a Django model class
    ::param fields is a list of field name strings.
    This method compares the lookups we want vs the lookups
    that are available. It ignores the unavailable fields we passed.
    """
    model_field_names = get_model_field_names(model)
    if fields is not None:
        """
        we'll iterate through all the passed field_names
        and verify they are valid by only including the valid ones
        """
        lookup_fields = []
        for x in fields:
            if "__" in x:
                # the __ is for ForeignKey lookups
                lookup_fields.append(x)
            elif x in model_field_names:
                lookup_fields.append(x)
    else:
        """
        No field names were passed, use the default model fields
        """
        lookup_fields = model_field_names
    return lookup_fields


def qs_to_dataset(qs, fields=None):
    """
    ::param qs is any Django queryset
    ::param fields is a list of field name strings, ignoring non-model field names
    This method is the final step, simply calling the fields we formed on the queryset
    and turning it into a list of dictionaries with key/value pairs.
    """

    lookup_fields = get_lookup_fields(qs.model, fields=fields)
    return list(qs.values(*lookup_fields))


def convert_to_dataframe(qs, fields=None, index=None):
    """
    ::param qs is an QuerySet from Django
    ::fields is a list of field names from the Model of the QuerySet
    ::index is the preferred index column we want our dataframe to be set to

    Using the methods from above, we can easily build a dataframe
    from this data.
    """
    lookup_fields = get_lookup_fields(qs.model, fields=fields)
    index_col = None
    if index in lookup_fields:
        index_col = index
    elif "id" in lookup_fields:
        index_col = "id"
    values = qs_to_dataset(qs, fields=fields)
    df = pd.DataFrame.from_records(values, columns=lookup_fields, index=index_col)
    return df


def convert_to_csv(qs, fields=None, index=None, filename=None) -> str:
    """
    ::param qs is an QuerySet from Django
    ::fields is a list of field names from the Model of the QuerySet
    ::return is a file object that can be written to a file or returned as a response
    """
    lookups = get_lookup_fields(qs.model, fields=fields)
    dataset = qs_to_dataset(qs, fields)
    rows_done = 0
    dataset = json.dumps(dataset, indent=4, sort_keys=True, default=str)
    writer = csv.DictWriter(fieldnames=lookups)
    writer.writeheader()
    for data_item in dataset:
        writer.writerow(data_item)
        rows_done += 1
    return writer


def generate_basic_report(queryset: dict, type: str) -> io.BytesIO:
    with open(convert_to_csv(queryset), "r") as csvfile:
        data = list(csv.reader(csvfile))

        styles = getSampleStyleSheet()
        report_title = Paragraph(f"{type.capitalize()}", styles["h1"])
        two_dem_array = []
        all_cells = [(0, 0), (-1, -1)]
        header = [(0, 0), (-1, 0)]
        column0 = [(0, 0), (0, -1)]
        column1 = [(1, 0), (1, -1)]
        column2 = [(2, 0), (2, -1)]
        column3 = [(3, 0), (3, -1)]
        column4 = [(4, 0), (4, -1)]
        table_style = TableStyle(
            [
                ("VALIGN", all_cells[0], all_cells[1], "TOP"),
                ("LINEBELOW", header[0], header[1], 1, colors.black),
                ("ALIGN", column0[0], column0[1], "LEFT"),
                ("ALIGN", column1[0], column1[1], "LEFT"),
                ("ALIGN", column2[0], column2[1], "LEFT"),
                ("ALIGN", column3[0], column3[1], "RIGHT"),
                ("ALIGN", column4[0], column4[1], "RIGHT"),
            ]
        )

    for index, row in enumerate(data):
        for col, val in enumerate(row):
            if col != 5 or index == 0:
                data[index][col] = val.strip("'[]()")
            else:
                data[index][col] = Paragraph(val, styles["Normal"])

        buffer = io.BytesIO()
        table = Table(data)
        table.setStyle(table_style)
        report = SimpleDocTemplate(buffer)
        report_content = [report_title, Spacer(1, 20), table]

    return report.build(report_content)
