from django import forms


class DetailedReportForm(forms.Form):
    student_first_name = forms.CharField(max_length=200, label="Student First Name")
    student_last_name = forms.CharField(max_length=200, label="Student Last Name")
    guardian = forms.CharField(max_length=200, label="Guardian Name")
    student_id = forms.IntegerField(label="Student ID")
    street_address = forms.CharField(max_length=180, label="Street Address")
    street_address_2 = forms.CharField(max_length=180, label="Street Address 2")
    city = forms.CharField(max_length=2, label="City")
    zip = forms.IntegerField(max_value=99999, label="Zip Code")
    student_date_of_birth = forms.DateField(label="Date of Birth")


class BasicReportForm(forms.Form):
    CHOICES = [
        ("IC", "In Care Report"),
        ("NIC", "Not In Care Report"),
        ("LPR", "Late Pickup Report"),
        ("AES", "All Enrolled Students"),
    ]
    report_type = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=CHOICES,
    )
