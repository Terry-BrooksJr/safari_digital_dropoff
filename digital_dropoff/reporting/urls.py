from django.urls import path
from . import views


urlpatterns = [
    path("basic-report/", views.basic_report, name="basic-report"),
    path("detailed-report/", views.detailed_report, name="detailed-report"),
    path("report-manager/", views.report_manager, name="report-manager"),
]
