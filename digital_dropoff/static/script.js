var isPinResetting = false;

function checkPinLength() {
  var input = document.querySelector("input[type='text']");

  var ivl = input.value.length;

  var pinPlaceholderItem = document.querySelectorAll(".pinPlaceholder span");

  if (ivl == 1) {
    pinPlaceholderItem[0].style.background = "red";
  } else if (ivl == 0) {
    pinPlaceholderItem[0].style.background = "none";
  }
  if (ivl == 2) {
    pinPlaceholderItem[1].style.background = "red";
  } else if (ivl == 1) {
    pinPlaceholderItem[1].style.background = "none";
  }
  if (ivl == 3) {
    pinPlaceholderItem[2].style.background = "red";
  } else if (ivl == 2) {
    pinPlaceholderItem[2].style.background = "none";
  }
  if (ivl == 4) {
    pinPlaceholderItem[3].style.background = "red";
  } else if (ivl == 3) {
    pinPlaceholderItem[3].style.background = "none";
  }
}

function inputAdd(number) {
  var input = document.querySelector("input[type='text']");
  if (input.value.length == 4) return;
  if (isPinResetting == true) return;
  input.value += number;
  checkPinLength();
}
const btn = document.querySelector("#btn");
const btnText = document.querySelector("#btnText");
const submit = document.querySelector("#submit");
btn.onclick = () => {
  var input = document.querySelector("input[type='text']");
  var ivl = input.value.length;
  if (ivl == 4) {
    btnText.innerHTML = "Checking Pin";
    btn.classList.add("active");
    sendPin();
  } else {
    alert(`Sorry! The pin is 4 digits, Your entry is ${4 - ivl} digits short. Please confirm pin and resubmit`);
    wrongCode()
  }
};
var input = document.body;
input.onkeydown = function () {
  var key = event.keyCode;
  if (key == 48) inputAdd(0);
  if (key == 49) inputAdd(1);
  if (key == 50) inputAdd(2);
  if (key == 51) inputAdd(3);
  if (key == 52) inputAdd(4);
  if (key == 53) inputAdd(5);
  if (key == 54) inputAdd(6);
  if (key == 55) inputAdd(7);
  if (key == 56) inputAdd(8);
  if (key == 57) inputAdd(9);
  if (key == 8) backspace();
};
console.log(input);
function backspace() {
  if (isPinResetting == true) return;
  var input = document.querySelector("input[type='text']");
  input.value = input.value.slice(0, -1);
  checkPinLength();
}

function wrongCode() {
  if (document.querySelector("input[type='text']").value == "") return;
  isPinResetting = true;
  var pinPlaceholderItem = document.querySelectorAll(".pinPlaceholder span");
  pinPlaceholderItem[3].style.background = "none";
  pinPlaceholderItem[3].style.transition = "0.6s";
  window.setTimeout(() => {
    pinPlaceholderItem[2].style.background = "none";
    pinPlaceholderItem[2].style.transition = "0.6s";
  }, 100);
  window.setTimeout(() => {
    pinPlaceholderItem[1].style.background = "none";
    pinPlaceholderItem[1].style.transition = "0.6s";
  }, 200);
  window.setTimeout(() => {
    pinPlaceholderItem[0].style.background = "none";
    pinPlaceholderItem[0].style.transition = "0.6s";
  }, 300);
  window.setTimeout(() => {
    pinPlaceholderItem[3].style.transition = "0.2";
    pinPlaceholderItem[2].style.transition = "0.2";
    pinPlaceholderItem[1].style.transition = "0.2";
    pinPlaceholderItem[0].style.transition = "0.2";
    isPinResetting = false;
  }, 310);
  document.querySelector("input[type='text']").value = "";
}
function sendPin() {
  const input = document.querySelector("input[type='text']");
  const pin = Number(input.value);
  console.log(pin);
}
const $studentItem = $(".student-item");
const $page = $(".page");
const $pagination = $(".pagination");
const $paginationList = $(".pagination__list");
const $studentSearch = $(".student__search");
const itemTotal = 10;

// hide all
function hideAll() {
  $studentItem.hide();
}

hideAll();

// display first 10
function displayRange(a, b) {
  hideAll();
  // display 0 - 1 students
  $studentItem.slice(a, b).fadeIn();
}

displayRange(0, itemTotal);

// create pagination links
let pagination = "";
for (var i = 0; i <= $studentItem.length / 10 - 1; i++) {
  pagination += `
    <li><span class ="pagination__num">${i}</span></li>
`;
}
$paginationList.append(pagination);
// click on pagination num
// pass into display range
// calc and show range
$("body").on("click", ".pagination__num", function () {
  hideAll();

  // get text number 1 - 5
  // get ranges for start and end
  let paginationText = Number($(this).text());
  let startFrom = paginationText * itemTotal + paginationText;
  let end = paginationText * itemTotal + paginationText + itemTotal;

  // display ranges
  displayRange(startFrom, end);
});

$studentSearch.on("input", function () {
  hideAll();

  $studentItem.each(function () {
    $(this).removeClass("result");
  });

  // value of searched
  var text = $(this).val().toLowerCase();
  // results of search
  var results = $("ul.student-list li:contains('" + text.toLowerCase() + "')");

  results.addClass("result");

  // if student has result class
  // dispaly
  // else hide

  if ($studentItem.hasClass("result")) {
    $(".result").show();
    $studentItem.removeClass("result");
  }
});

$studentSearch.keyup(function () {
  if (!this.value) {
    hideAll();
    displayRange(0, itemTotal);
  }
});