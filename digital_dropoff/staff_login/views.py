from django.shortcuts import render, redirect
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponse, HttpRequest
from roster.views import roster

# Create your views here.
def login_user(request: HttpRequest) -> HttpResponse:
    context = {'form': LoginForm()}
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            messages.success(request, 'You have successfully logged in')
            login(request, user)
            return redirect ('/roster')
        else:
            messages.error(request, 'Invalid username or password')
            return render(request, 'login.html', context)
    else: 
        return render (request, 'login.html', context)
