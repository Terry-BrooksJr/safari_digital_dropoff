from django.db import models
import bcrypt

class Teachers(models.Model):
    username = models.CharField(max_length=20, unique=True)
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    password = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    
    def __str__(self):
        return f"{self.last_name}, {self.first_name} ({self.username})"
    
    def hash_password(self, password):
        self.password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
        self.save()
        
    def check_password(self, password):
        return bcrypt.checkpw(password.encode(), self.password.encode())